## Static Site Generator

This is a very simple static website generator that uses Jinja templates
to embed HTML into HTML.
- The examples in `src` and `static` should be fairly self-explanatory
- Running `./compile.py` compiles your website into `dist`.
- The `fragments` directory contains templates that can be extended or
  included by the files in `src`.

## Why another one instead of Pelican, Hyde, etc...?

I personally find the conversion of markdown to HTML to just be another
layer of abstraction to debug.  IMHO this project is a simple solution to a
simple problem for a simple use-case, and does not seem to be adequately
addressed by any existing solution I could find.  I'm also a back-end guy
with limited front-end skills, so I don't pretend to understand the
motivations/wants/needs of people who can make much prettier front-ends
than I can.

If you like Jinja as a templating language and prefer to store your HTML as
HTML, then you will probably enjoy this project.
