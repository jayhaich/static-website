#!/usr/bin/env python3

import os
import sys


def _(x):
    print(x)
    retcode = os.system(x)
    assert not retcode, retcode


def main():
    file_path = os.path.abspath(__file__)
    dirname = os.path.dirname(file_path)
    os.chdir(dirname)

    name = input("Enter the new project name: ").strip()
    assert name
    assert name.islower(), name
    assert name.replace("_", "").isalpha(), name
    assert len(name) < 20, len(name)

    # Delete Python bytecode files
    _("find . -name '*.pyc' -delete")
    # Replace 'static-site-name' with $name in files
    _(
        "find src/ fragments/ static/ "
        "-type f "
        "| xargs sed -i 's/static-site-name/{name}/gI'".format(name=name)
    )


if __name__ == "__main__":
    main()
